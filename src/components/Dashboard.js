import React, { Component } from 'react'
import "./Dashboard.css"

import { 
    createReadingList, 
    determineRedLowLimit, 
    determineRedHighLimit 
} from '../utils'

export class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
           redLowLimit: null,
           redHighLimit: null 
        }
    }
    
    
    readFile = async (e) => {
        e.preventDefault()
        const reader = new FileReader()
        reader.onload = async (e) => { 
          const text = (e.target.result)
          
          // Split all line in input file
          const allLines = text.split('\n')
        
          const readings = createReadingList(allLines)
          
          const redLowLimit = determineRedLowLimit(readings)
          console.log("Red Low Limit: ", redLowLimit)

          const redHighLimit = determineRedHighLimit(readings)
          console.log("Red High Limit: ", redHighLimit)

          this.setState({
            redLowLimit,
            redHighLimit
          })

        };
        reader.readAsText(e.target.files[0])
    }

    

    render() {
        const {redLowLimit, redHighLimit} = this.state;
        
        return (
            <div className="Upload" >
                <input type="file" onChange={(e) => this.readFile(e)} />
                <div className="Output">
                    { redLowLimit && <pre>{JSON.stringify(redLowLimit, null, 2)}</pre>}

                    { redHighLimit && <pre>{JSON.stringify(redHighLimit, null, 2)}</pre>}
                </div>
            </div>
        )
    }
}

export default Dashboard
