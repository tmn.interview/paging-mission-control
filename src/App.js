
import './App.css';

import Dashboard from './components/Dashboard'


function App() {
  return (
    <div className="App">
      <div className="Title" >
        <h1 >Paging Mission Control</h1>
        <hr/>
      </div>
      
      <Dashboard></Dashboard>
      
     </div>
  );
}

export default App;