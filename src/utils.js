/**
 * 
 * @param {*} satelliteId  - The Satellite ID
 * @param {*} severity  - The severity
 * @param {*} component - The component
 * @param {*} timestamp  - The timestamp
 * @returns - An object that represents the alert message
 */
function AlertMessage(satelliteId, severity, component, timestamp) {
    return {
        satelliteId,
        severity,
        component,
        timestamp
    }
}

/**
 * 
 * @param {*} timestamp  - The timestamp
 * @param {*} satelliteId - The Satellite ID
 * @param {*} redHighLimit  - The red high limit for the thermostat
 * @param {*} yellowHighLimit - The yellow high limit for the 
 * @param {*} yellowLowLimit  - The yellow low limit
 * @param {*} redLowLimit - The red low limit
 * @param {*} rawValue - The raw value
 * @param {*} component  - The component
 * @returns - An object that represents a single line of data in the input file.
 */
function StatusTelemetryData(timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component ) {
    const dateArr = timestamp.split(" ");
    const yearStr = dateArr[0]
    const timeStr = (dateArr[1]).split(':')

    const readingDate = new Date(
        yearStr.substring(0,4),
        yearStr.substring(4,6),
        yearStr.substring(6),
        timeStr[0],
        timeStr[1],
        timeStr[2].substring(0,2)
        )
    return {
        "timestampCal": Date.parse(readingDate), 
        "timestampFormatted": yearStr.substring(0,4) + "-" + yearStr.substring(4,6) + "-" + yearStr.substring(6) + "T" + timeStr[0] + ":" + timeStr[1]+ ":" + timeStr[2]+"Z",
        timestamp,
        satelliteId,
        "redHighLimit": parseFloat(redHighLimit),
        "yellowHighLimit": parseFloat(yellowHighLimit),
        "yellowLowLimit": parseFloat(yellowLowLimit),
        "redLowLimit": parseFloat(redLowLimit),
        "rawValue": parseFloat(rawValue),
        component
    }
}
/**
 * Create an object whose key is the satellite Id and value is the list of readings
 * 
 * @param {*} readings - A list of all the readings from the input text file
 * @returns An object whose key is the satellite Id and value is the list of readings
 */
function createReadingList(readings) {
    let data = {}
    for(let reading of readings){
        let temp = reading.split('|');
        if(!data[temp[1]]) {
            data[temp[1]] = [new StatusTelemetryData(temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7])]
        } else {
            data[temp[1]].push(new StatusTelemetryData(temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7]))
        }
    }

    return data;
}

/**
 * Gets the best reading that corresponds to the red high limit
 * 
 * @param {*} readings - A list or readings
 * @returns - The reading which satisfies the condition to create the red high limit alert message.
 */
function getReadingThatExceedRedHighLimit(readings) {
    let result = [];

    // Search for all the Thermostat readings that are greater than red high limit
    for(let reading of readings) {
        if(reading.rawValue > reading.redHighLimit && reading.component === 'TSTAT' ) {
          result.push(reading)
        }
    }
    
    // There should be atleat 3 thermostat readings
    if(result.length >= 3) {

        // Sorting from min to max time
        result.sort((x,y) => parseInt(x.timestampCal) - parseInt(y.timestampCal))
        /**
         * check if difference in time of the first and the third sorted 
         * readings is less that 5 minutes (300000)
         */

        if( (result[2].timestampCal - result[0].timestampCal) < 300000) {
            let threeReadingsWithLowestTime = result.slice(0,3)

            /**
             * Sort in descending order and get the readings with the 
             * highiest Thermostat readings in the first three
             */
            threeReadingsWithLowestTime.sort((x,y) => parseInt(y.rawValue) - parseInt(x.rawValue))

            return threeReadingsWithLowestTime[0]
        }
        
    } else {
        console.log("Lesser than 3 readings were recorded!")
        return ;
    }    
}

/**
 *  Determines the red high limit
 * 
 * @param {*} readings - Object whose key is the satellite Id and value is the list of readings for that satellite
 * @returns The alert message or nothing.
 */
function determineRedHighLimit(readings) {
    for (const [satelliteId, entries] of Object.entries(readings)) {
        const result = getReadingThatExceedRedHighLimit(entries)
        if(result) {
            const {component, timestampFormatted} = result;
            return new AlertMessage(satelliteId, "RED HIGH", component, timestampFormatted)
        }
    }
    return ;
}

function getReadingUnderRedlowLimit(readings) {
    let result = [];

    // Search for all the battery voltage readings that are lower than red low limit
    for(let reading of readings) {
        if(reading.rawValue < reading.redLowLimit && reading.component === 'BATT' ) {
          result.push(reading)
        }
    }
    
    // There should be atleat 3 battery voltage readings
    if(result.length >= 3) {

        // Sorting from min to max time
        result.sort((x,y) => parseInt(x.timestampCal) - parseInt(y.timestampCal))
        /**
         * check if difference in time of the first and the third sorted 
         * readings is less that 5 minutes (300000)
         */

        if( (result[2].timestampCal - result[0].timestampCal) < 300000) {
            let threeReadingsWithLowestTime = result.slice(0,3)

            /**
             * Sort in descending order and get the readings with the 
             * highiest Thermostat readings in the first three
             */
            threeReadingsWithLowestTime.sort((x,y) => parseInt(y.rawValue) - parseInt(x.rawValue))

            return threeReadingsWithLowestTime[0]
        }
        
    } else {
        console.log("Lesser than 3 readings were recorded!")
        return ;
    }   
}

/**
 *  Determines the red low limit
 * 
 * @param {*} readings - Object whose key is the satellite Id and value is the list of readings for that satellite
 * @returns The alert message or nothing.
 */
 function determineRedLowLimit(readings) {
    for (const [satelliteId, entries] of Object.entries(readings)) {
        const result = getReadingUnderRedlowLimit(entries)

        if(result) {
            const {component, timestampFormatted} = result;
            return new AlertMessage(satelliteId, "RED LOW", component, timestampFormatted)
        }
    }
    return ;
}

export {
    StatusTelemetryData,
    createReadingList,
    determineRedLowLimit,
    determineRedHighLimit
}